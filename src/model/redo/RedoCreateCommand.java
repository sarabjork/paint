package model.redo;

import model.ModelFacade;
import model.undo.UndoCreateCommand;


public class RedoCreateCommand implements RedoCommand {
	private ModelFacade model;

	public RedoCreateCommand(ModelFacade model ){
		this.model = model;
	}

	@Override
	public void exceute() {
		if(model.deletedShapes.size()!=0) {
			model.shapes.add(model.deletedShapes.get(model.deletedShapes.size() - 1));
			model.deletedShapes.remove(model.deletedShapes.size() - 1);

			UndoCreateCommand undoCreateCommand = new UndoCreateCommand(model);
			model.undoInvoker.addCommandToStack(undoCreateCommand);
		}
	}
}
