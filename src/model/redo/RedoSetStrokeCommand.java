package model.redo;

import model.shapes.Shape;
import model.undo.UndoCommand;
import model.undo.UndoDeleteCommand;
import model.undo.UndoInvoker;
import model.undo.UndoSetStrokeCommand;

public class RedoSetStrokeCommand implements RedoCommand {
	private String stroke;
	private Shape shape;
	RedoInvoker redoInvoker;
	UndoInvoker undoInvoker;

	public RedoSetStrokeCommand(Shape shape, String stroke, RedoInvoker redoInvoker, UndoInvoker undoInvoker) {
		this.shape = shape;
		this.stroke = stroke;
		this.redoInvoker = redoInvoker;
		this.undoInvoker = undoInvoker;
	}


	@Override
	public void exceute() {
		UndoSetStrokeCommand undoSetStrokeCommand = new UndoSetStrokeCommand(shape,shape.getCurrentStroke(),redoInvoker,undoInvoker);
		undoInvoker.addCommandToStack(undoSetStrokeCommand);
		 shape.setCurrentStroke(stroke);
	}
}