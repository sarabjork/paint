package model.redo;

import java.util.Stack;

public class RedoInvoker {
	Stack<RedoCommand> commands;

	public RedoInvoker(){
		this.commands = new Stack<>();
	}

	public void addCommandToStack(RedoCommand command){
		commands.push(command);
	}

	public void clearStack(){
		commands.clear();
	}

	public void executeCommand(){
		if(!commands.empty()){
			RedoCommand command = commands.pop();
			command.exceute();
		}
	}

}
