package model.redo;

import model.shapes.FillableShape;
import model.shapes.Shape;
import model.undo.UndoCommand;
import model.undo.UndoInvoker;
import model.undo.UndoSetFilledCommand;


public class RedoSetFilledCommand implements RedoCommand {
	private boolean filled;
	private Shape shape;
	RedoInvoker redoInvoker;
	UndoInvoker undoInvoker;

	public RedoSetFilledCommand(Shape shape, boolean filled, RedoInvoker redoInvoker, UndoInvoker undoInvoker) {
		this.shape = shape;
		this.filled = filled;
		this.redoInvoker = redoInvoker;
		this.undoInvoker = undoInvoker;
	}


	@Override
	public void exceute() {
		((FillableShape) shape).setFilled(filled);

		UndoSetFilledCommand undoSetFilledCommand = new UndoSetFilledCommand(shape,!filled,redoInvoker,undoInvoker);
		undoInvoker.addCommandToStack(undoSetFilledCommand);
	}
}
