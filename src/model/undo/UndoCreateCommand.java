package model.undo;

import model.ModelFacade;
import model.redo.*;


public class UndoCreateCommand implements UndoCommand {
	private ModelFacade model;

	public UndoCreateCommand(ModelFacade model ){
		this.model = model;
	}

	@Override
	public void exceute() {
		if(model.shapes.size()!=0){
			model.deletedShapes.add(model.shapes.get(model.shapes.size() - 1));
			model.shapes.remove(model.shapes.size() - 1);

			RedoCreateCommand redoCreateCommand = new RedoCreateCommand(model);
			model.redoInvoker.addCommandToStack(redoCreateCommand);
		}
	}
}