package model;

import model.redo.RedoInvoker;
import model.shapes.*;
import model.shapes.Shape;
import model.undo.*;
import observer.Observer;
import observer.Subject;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

public class ModelFacade implements Subject {

    private ArrayList<Observer> observers;
    public ArrayList<Shape> shapes, deletedShapes;
    private Shape sampleRectangle, sampleCircle, sampleLine, selectDot, tempShape;
    private ShapeFactory shapeFactory;

    public UndoInvoker undoInvoker;
    public RedoInvoker redoInvoker;

    public ModelFacade() {
        shapeFactory = new ShapeFactory();

        undoInvoker = new UndoInvoker();
        redoInvoker = new RedoInvoker();

        observers = new ArrayList<>();

        shapes = new ArrayList<>();
        deletedShapes = new ArrayList<>();

        shapeFactory = new ShapeFactory();

        sampleRectangle = new model.shapes.Rectangle(0,0,0,0,"Normal", false);
        sampleCircle = new Circle(0,0,0,0,"Normal", false);
        sampleLine = new Line(0,0,0,0,"Normal");

        makeDot();

    }

    public void makeDot(){
        selectDot = shapeFactory.getClone(sampleCircle);

        selectDot.setCurrentStroke("Normal");
        selectDot.setColor(Color.RED);
        ((FillableShape) selectDot).setWidth(10);
        ((FillableShape) selectDot).setHeight(10);
        ((FillableShape) selectDot).setFilled(true);

        //osynlig
        selectDot.setX(-100);
        selectDot.setY(-100);
    }

    private Shape createShape(String shapeName, Point startPos, Point endPos, String strokeStyle, boolean filled){
        Shape newShape = null;

        switch (shapeName){
            case "Circle":
                newShape = shapeFactory.getClone(sampleCircle);
                break;
            case "Rectangle":
                newShape = shapeFactory.getClone(sampleRectangle);
                break;
            case "Line":
                newShape = shapeFactory.getClone(sampleLine);
                newShape.setX((int) startPos.getX());
                newShape.setY((int) startPos.getY());
                newShape.setX2((int) endPos.getX());
                newShape.setY2((int) endPos.getY());
                newShape.setCurrentStroke(strokeStyle);
                break;
        }

        if(newShape instanceof FillableShape) {

            int x = (int) Math.min(startPos.getX(), endPos.getX());
            int x2 = (int) Math.max(startPos.getX(), endPos.getX());
            int y = (int) Math.min(startPos.getY(), endPos.getY());
            int y2 = (int) Math.max(startPos.getY(), endPos.getY());

            newShape.setX(x);
            newShape.setY(y);
            newShape.setX2(x2);
            newShape.setY2(y2);
            newShape.setCurrentStroke(strokeStyle);

            ((FillableShape) newShape).setWidth(Math.abs(x - x2));
            ((FillableShape) newShape).setHeight(Math.abs(y - y2));
            ((FillableShape) newShape).setFilled(filled);
        }
        return newShape;
    }

    public void drawShape(String shapeName, Point startPos, Point endPos, String strokeStyle, boolean filled){
        if(shapeName!=null) {
            tempShape = createShape(shapeName, startPos, endPos, strokeStyle, filled);
            notifyObservers();
        }
    }

    public void addShape(String shapeName, Point startPos, Point endPos, String strokeStyle, boolean filled){
        Shape newShape = createShape(shapeName, startPos, endPos, strokeStyle, filled);

        tempShape = null;
        shapes.add(newShape);

        //clear the redo when new shape is created
        redoInvoker.clearStack();

        UndoCreateCommand undoCreateCommand = new UndoCreateCommand(this);
        undoInvoker.addCommandToStack(undoCreateCommand);

        notifyObservers();
    }

    public void deleteShape(Shape shape){
        if(shape!=null) {
            deletedShapes.add(shape);
            shapes.remove(shape);

            UndoDeleteCommand undoDeleteCommand = new UndoDeleteCommand(this);
            undoInvoker.addCommandToStack(undoDeleteCommand);

            notifyObservers();
        }
    }

    public void setFilled(FillableShape shape, boolean filled){
        if(shape!=null) {
            UndoSetFilledCommand undoSetFilledCommand = new UndoSetFilledCommand(shape, shape.isFilled(), redoInvoker, undoInvoker);
            undoInvoker.addCommandToStack(undoSetFilledCommand);

            shape.setFilled(filled);
            notifyObservers();
        }
    }

    public void setStrokeStyle(Shape shape, String strokeStyle){
        if(shape!=null) {
            UndoSetStrokeCommand undoSetStrokeCommand = new UndoSetStrokeCommand(shape, shape.getCurrentStroke(), redoInvoker, undoInvoker);
            undoInvoker.addCommandToStack(undoSetStrokeCommand);

            shape.setCurrentStroke(strokeStyle);
            notifyObservers();
        }
    }

    public void undo(){
        undoInvoker.executeCommand();
        notifyObservers();
    }

    public void redo(){
        redoInvoker.executeCommand();
        notifyObservers();
    }

    public void saveToFile(){
        InputOutput.serialize(shapes);
    }

    public void loadFromFile(){
        ArrayList<Shape> newShapes = InputOutput.deSerialize();

        if(newShapes!=null){
            shapes = newShapes;
            notifyObservers();
            redoInvoker.clearStack();
            undoInvoker.clearStack();
        }
        else
            JOptionPane.showMessageDialog(new Frame(), "File not found.");

    }

    public void changeColor(Shape shape, Color color){
        if(shape!=null && color!=null) {
            shape.setColor(color);
            notifyObservers();
        }
    }

    public Shape getShapeByCoordinates(Point point){
        Shape selectedShape = null;
        for(int i=0; i<shapes.size();i++){
            if(shapes.get(i).isHit(point.getX(), point.getY())){
                selectedShape = shapes.get(i);
                break;
            }
        }
        return selectedShape;
    }

    public void markShape(Shape selectedShape) {
        if(selectedShape!=null) {
            if (!selectedShape.equals(selectDot)) {  //om vi inte har valt själva markeringen
                if (selectedShape instanceof FillableShape) {
                    selectDot.setX(selectedShape.getX() + (((FillableShape) selectedShape).getWidth()/2 - 5));
                    selectDot.setY(selectedShape.getY() + (((FillableShape) selectedShape).getHeight()/2 - 5));
                } else { //if a line
                    selectDot.setX((selectedShape.getX2() + selectedShape.getX()) / 2);
                    selectDot.setY((selectedShape.getY2() + selectedShape.getY()) / 2);
                }
                notifyObservers();
            }
        }
    }

    @Override
    public void register(Observer obs) {
        if(!observers.contains(obs)){
            observers.add(obs);
        }
    }

    @Override
    public void unregister(Observer obs) {
        if(observers.contains(obs)){
            observers.remove(obs);
        }
    }

    @Override
    public void notifyObservers() {
        for(Observer obs : observers){
            obs.update(shapes, selectDot, tempShape);
        }
    }



}
