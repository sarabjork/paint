package model.shapes;


//PROTOTYPE PATTERN
public class ShapeFactory {

	public Shape getClone(Shape shapeSample){
		return shapeSample.createCopy();
	}

}
