package model;


import model.shapes.Shape;

import java.io.*;
import java.util.ArrayList;


public class InputOutput {

	public static void serialize(ArrayList<Shape> shapes){


		try{
			FileOutputStream fos= new FileOutputStream("myfile");
			ObjectOutputStream oos= new ObjectOutputStream(fos);
			oos.writeObject(shapes);
			oos.close();
			fos.close();
		}catch(IOException ioe){
			ioe.printStackTrace();
		}
	}

	public static ArrayList<Shape> deSerialize(){

		ArrayList<Shape> shapes;
		try
		{
			FileInputStream fis = new FileInputStream("myfile");
			ObjectInputStream ois = new ObjectInputStream(fis);
			shapes = (ArrayList<Shape>) ois.readObject();
			ois.close();
			fis.close();
		}catch(IOException ioe){
			System.out.println("FILE NOT FOUND");
			return null;
		}catch(ClassNotFoundException c){
			System.out.println("Class not found");
			return null;
		}

		return shapes;
	}

}
