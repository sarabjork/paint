package observer;

import java.util.ArrayList;

public interface Subject {

    void register(Observer obs);
    void unregister(Observer obs);
    void notifyObservers();

}
