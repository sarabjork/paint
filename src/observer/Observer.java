package observer;

import model.shapes.Shape;

import java.util.ArrayList;

public interface Observer {

    void update(ArrayList<Shape> shapes, Shape selectDot, Shape tempShape);

}
