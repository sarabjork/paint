import view.MainFrame;
import javax.swing.*;


public class Main {

    public static void main(String[] args){
        MainFrame mainframe = new MainFrame();

        mainframe.setTitle("Paint v3.0 beta");
        mainframe.setSize(1000, 750);
        mainframe.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        mainframe.setVisible(true);
    }

}
