package view;

import controller.Controller;

import javax.swing.*;
import java.awt.*;

public class ToolPanel extends JPanel {

    private Controller controller;

    public ToolPanel(Controller controller) {
        this.controller = controller;

        setLayout(new GridLayout(3, 1, 0, 0));
        add(new Panel1(controller));
        add(new Panel2(controller));
        add(new Panel3(controller));

    }

}
