package view;

import controller.Controller;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class Panel3 extends JPanel {

	private JButton saveButton, loadButton, undoButton, redoButton, colorButton;
	private Controller controller;

	public Panel3(Controller controller){
		this.controller = controller;
		setLayout(new GridLayout(3, 2, 0, 0));

        saveButton = new JButton("Save");
        loadButton = new JButton("Load");
        undoButton = new JButton("Undo");
        redoButton = new JButton("Redo");
		colorButton = new JButton("Color");

        addComponents();
        addButtonListeners();
	}

	private void addComponents(){
		this.add(saveButton);
		this.add(loadButton);
		this.add(undoButton);
		this.add(redoButton);
		this.add(colorButton);
	}

	private void addButtonListeners(){
		saveButton.addActionListener(controller);
		loadButton.addActionListener(controller);
		undoButton.addActionListener(controller);
		redoButton.addActionListener(controller);
		colorButton.addActionListener(controller);
	}

}
